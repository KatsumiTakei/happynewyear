#include "ImGuiWrapper.hpp"

#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx11.h"

class Window final {
public:

	Window(const char* label, ImVec2 pos, ImVec2 size)
		:titleLabel(label), pos(pos), size(size){

	}
	~Window() {
		ImGui::ClearWindowSettings(titleLabel);
	}

	void Update() {
		if (contents.empty()) {
			return;
		}

		initWindow.Call([&]() {
			ImGui::SetNextWindowPos(pos);
			ImGui::SetNextWindowSize(size);
		});

		ImGui::Begin(titleLabel, nullptr, ImGuiWindowFlags_NoSavedSettings);
		for (const auto& func : contents) {
			func.first();
		}
		ImGui::End();

		contents.remove_if([](std::pair<std::function<void(void)>, bool> contents) {
			return contents.second;
		});
	}

	std::list<std::pair<std::function<void(void)>, bool>> contents;
	const char* titleLabel = "";
	ImVec2 pos, size;
	DoOnce initWindow;
};


class ImGuiWrapper::Impl {
public:
	std::list<Window> windows;
};


ImGuiWrapper::ImGuiWrapper() 
	: pImpl(std::make_unique<Impl>()){
}

ImGuiWrapper::~ImGuiWrapper(){
}

void ImGuiWrapper::RegistWindow(const char* label, std::initializer_list<float> pos, std::initializer_list<float> size) {
	pImpl->windows.emplace_back(label, ImVec2(pos.begin()[0], pos.begin()[1]), ImVec2(size.begin()[0], pos.begin()[1]));
}

void ImGuiWrapper::PushContents(const char* windowLabel, std::function<void(void)> internal, bool isRemove) {
	for (auto& window : pImpl->windows) {
		if (FNVHash::hash(window.titleLabel) != FNVHash::hash(windowLabel)) {
			continue;
		}
		window.contents.emplace_back(internal, isRemove);
	}
}

void ImGuiWrapper::Update() {
	for (auto& window : pImpl->windows) {
		window.Update();
	}
}
