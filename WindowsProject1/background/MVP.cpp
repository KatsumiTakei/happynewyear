#include "MVP.hpp"

View::View(const char* name, ImVec2 pos, ImVec2 size)
	: viewName(name), pos(pos), size(size), state(State::kInvisible) {
}

View::~View(){
}

void View::StartFadeIn() {
	state = State::kFadeIn;
	totalDeltaTime = 0;
}

void View::StartFadeOut() {
	state = State::kFadeOut;
	totalDeltaTime = 0;
}

//	このあたりの実装は適当
void View::Update() {
	ImGuiManager::Get()->PushContents(kLog, [&] {
		ImGui::Text("%s state=%d", viewName, state);
	});
	constexpr uint8_t kFadeTime = 1;
	totalDeltaTime += Singleton<QueryPerformanceTimer>::Get()->DeltaTime();

	ImGui::SetNextWindowPos(pos);
	ImGui::SetNextWindowSize(size);

	switch (state) {
	case State::kInvisible: {
		ImGui::SetNextWindowBgAlpha(0);
	}break;
	case State::kFadeIn: {
		ImGui::SetNextWindowBgAlpha(totalDeltaTime / kFadeTime);
		DelayManager::Execute(viewName, kFadeTime, Delay::MeasureType::kSeconds, [&](){ 
			totalDeltaTime = 0;
			state = State::kUpdate;
		});
	}break;
	case State::kUpdate: {
	}break;
	case State::kFadeOut: {
		ImGui::SetNextWindowBgAlpha(kFadeTime - kFadeTime * totalDeltaTime);
		DelayManager::Execute(viewName, kFadeTime, Delay::MeasureType::kSeconds, [&]() { state = State::kInvisible; });
	}break;
	}
	ImGui::Begin(viewName);
	CoreUpdate();
	ImGui::End();
}
