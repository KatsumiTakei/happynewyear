#pragma once

#include <initializer_list>

/**
 * @brief まともなのを作るのは面倒なので雑なラッパー
*/
class ImGuiWrapper final {

private:
	class Impl;
	std::unique_ptr<Impl> pImpl;

public:

	ImGuiWrapper();
	~ImGuiWrapper();

public:
	void RegistWindow(const char* label, std::initializer_list<float> pos, std::initializer_list<float> size);
	void PushContents(const char* windowLabel, std::function<void(void)> internal, bool isRemove = true);
	void Update();

};

static constexpr const char* kMain = "Main";
static constexpr const char* kLog = "Log";
static constexpr const char* kCommandHistory = "CommandHistory";

template<typename T>
class Singleton;
using ImGuiManager = Singleton<ImGuiWrapper>;