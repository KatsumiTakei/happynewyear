#pragma once

#include <Windows.h>
#include <iostream>
#include <memory>
#include <functional>
#include <string>

#include "Keyboard.hpp"
#include "Singleton.hpp"
#include "Scene.hpp"
#include "Delay.hpp"
#include "QueryPerformanceTimer.hpp"
#include "ImGuiWrapper.hpp"
#include "DoOnce.hpp"
#include "Delay.hpp"


template <typename ... Args>
std::string Format(const std::string& fmt, Args ... args)
{
    size_t len = std::snprintf(nullptr, 0, fmt.c_str(), args ...);
    std::vector<char> buf(len + 1);
    std::snprintf(&buf[0], len + 1, fmt.c_str(), args ...);
    return std::string(&buf[0], &buf[0] + len);
}