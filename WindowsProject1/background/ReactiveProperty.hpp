#pragma once

template <typename T>
class ReactiveProperty final {
public:
	ReactiveProperty(T value)
		: value(value) {
	}
	ReactiveProperty() = default;
	~ReactiveProperty() = default;

	void Subscribe(std::function<void(T)> func) {
		onChangeValue = func;
	}
	void OnNext(const T& newValue) { 
		value = newValue;
		if (onChangeValue) {
			onChangeValue(value);
		}
	}
	T GetValue()const { return value; }

private:
	std::function<void(T)> onChangeValue;
	T value;
};