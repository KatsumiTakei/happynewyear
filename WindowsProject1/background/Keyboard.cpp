#include "Keyboard.hpp"
#include <Windows.h>

//#define CONSOLE

template<typename T, std::size_t N>
std::size_t GetLength(const T (&)[N]){
	return N;
}

Keyboard::Keyboard()
{
	ZeroMemory(&now_, sizeof(now_));
	ZeroMemory(&old_, sizeof(old_));
}

Keyboard::~Keyboard() = default;

void Keyboard::RregisterKey(eKey argkeycode, unsigned char argBytecode)
{
	keyboard_.emplace(argkeycode, argBytecode);
}

bool Keyboard::Update()
{
	old_ = now_;

#if !defined(CONSOLE)
	GetKeyboardState(now_);
#else
	for (int i = 0; i < GetLength(now_.key); i++) {
		now_[i] = GetKeyState(i);
	}
#endif

	return true;
}

bool Keyboard::IsPressed(eKey argKeyCode)
{
	return (now_[keyboard_.at(argKeyCode)] & 0x80);
}

bool Keyboard::IsTouched(eKey argKeyCode)
{
	bool oldkey = (old_[keyboard_.at(argKeyCode)] & 0x80);
	bool nowkey = (now_[keyboard_.at(argKeyCode)] & 0x80);

	return (!oldkey && nowkey);
}