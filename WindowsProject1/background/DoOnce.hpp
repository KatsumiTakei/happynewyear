﻿/**
 * @file DoOnce.h
 * @data 2021 / 12 / 7
 * @author	
 * Copyright (c) . All rights reserved.
 */
#pragma once

#include <functional>

 /**
  *  @class  DoOnce
  *  @brief  一度しか関数を呼ばない関数オブジェクト
  */
class DoOnce final {

public:
    DoOnce(bool start_close = false) : is_once_(start_close) {
    }
    ~DoOnce() = default;

public:

    /**
     *  @class  Reset
     *  @brief  状態をリセットする
     */
    void Reset() {
        is_once_ = false;
    }

    /**
     *  @fn     Call
     *  @brief  引数の関数を一度のみ実行する
     *  @param  (method)  !<  一度のみ実行する関数
     *  @param  (reset)  !<  状態をリセットするかどうか
     */
    void Call(std::function<void()> method, bool reset = false) {

        if (!is_once_) {
            is_once_ = true;
            std::invoke(method);
        }
        if (reset) {
            is_once_ = false;
        }
    }

public:

    /**
     *  @fn     operator()
     *  @brief  引数の関数を一度のみ実行する
     *  @param  (method)  !<  一度のみ実行する関数
     *  @param  (reset)  !<  状態をリセットするかどうか
     */
    void operator()(std::function<void()> method, bool reset = false) {

        Call(method, reset);
    }

private:
    bool is_once_;//  !<  関数を呼んだかどうか
};
