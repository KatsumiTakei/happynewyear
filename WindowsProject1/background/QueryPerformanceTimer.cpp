﻿/**
 *	@file	QueryPerformanceTimer.cpp
 *	@date	2017 / 10 / 04
 *	@author	Katsumi Takei
 *	Copyright (c) Kastumi Takei. All rights reserved.
 */
#include "QueryPerformanceTimer.hpp"
#include <windows.h>
#include <assert.h>

static float sFrameRate = 0.0f;

class QueryPerformanceTimer::Impl
{
public:
	Impl()
	{
		Reset();
	}
	~Impl() = default;

private:
	void Check()
	{
		oldTick_ = lastTick_;
		QueryPerformanceCounter(&lastTick_);

		totalTick_.QuadPart += lastTick_.QuadPart - oldTick_.QuadPart;
		if (--checkTimeCnt_ <= 0)
		{//	フレームレートの更新
			checkTimeCnt_ = checkFrame_;
			sFrameRate = checkFrame_ / (static_cast<float>(totalTick_.QuadPart) / frequency_.QuadPart);
			totalTick_.QuadPart = 0;
		}
	}
public:
	void Wait()
	{
		LARGE_INTEGER now;

		QueryPerformanceCounter(&now);

		while ((frequency_.QuadPart / static_cast<long long>(checkFrame_)) > (now.QuadPart - lastTick_.QuadPart))
		{//	現在時間の更新
			QueryPerformanceCounter(&now);
		}
		deltaTime_ = (now.QuadPart - lastTick_.QuadPart) / static_cast<float>(frequency_.QuadPart);
		Check();
	}

	void Reset(int argFrameRate = 60)
	{
		deltaTime_ = 0.0;
		BOOL canUse = QueryPerformanceFrequency(&frequency_);
		assert(canUse && "Can't use QueryPerformanceFrequency...");
		QueryPerformanceCounter(&lastTick_);
		oldTick_ = lastTick_;

		totalTick_.QuadPart = 0;
		sFrameRate = 0.0f;
		checkFrame_ = argFrameRate;
		checkTimeCnt_ = checkFrame_;
	}

private:
	LARGE_INTEGER lastTick_;
	LARGE_INTEGER oldTick_;
	LARGE_INTEGER frequency_;
	LARGE_INTEGER totalTick_;
	int checkTimeCnt_;
	int checkFrame_;
public:
	float deltaTime_;
};

QueryPerformanceTimer::QueryPerformanceTimer()
	:pImpl(std::make_unique<Impl>())
{
}

QueryPerformanceTimer::~QueryPerformanceTimer() = default;

void QueryPerformanceTimer::Wait()
{
	pImpl->Wait();
}

void QueryPerformanceTimer::Reset(int argFrameRate)
{
	pImpl->Reset(argFrameRate);
}

float QueryPerformanceTimer::FramePerSecond() 
{
	return sFrameRate;
}

float QueryPerformanceTimer::DeltaTime() const
{
	return pImpl->deltaTime_;
}