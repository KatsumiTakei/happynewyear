#pragma once

#include "FNVHash.hpp"
#include <unordered_map>
#include <memory>

enum class SceneState {
	kDead,
	kLoading,
	kUsable,
	kTerminating,
};

class Scene abstract {
public:
	Scene(const char* name);
	~Scene();
public:
	void Load();
	void Update();
	void Terminate();
	SceneState GetSceneState()const { return state_; };

protected:
	virtual void CoreUpdate() = 0;

private:
	const char* name_ = "";
	SceneState state_ = SceneState::kDead;
};

class SceneApi final {
public:
	template<class T>
	static T* FindScene(const char* name) {
		auto hash = FNVHash::hash(name);
		if (hashMap_.find(hash) == hashMap_.end()) {
			hashMap_.emplace(hash, std::make_unique<T>());
		}
		return static_cast<T*>(hashMap_.at(hash).get());
	}

	static void Update();
private:
	static std::unordered_map<size_t, std::unique_ptr<Scene>> hashMap_;
};
