#pragma once

#include <unordered_map>

class Delay final {

public:
	enum class MeasureType {
		kFrame,
		kSeconds,
	};
public:
	Delay(MeasureType type)
		: type(type), totalTime(0) {
	}
public:
	void StepTime(float deltaTime) {
		switch (type) {
		case MeasureType::kFrame: {
			totalTime++;
		}break;
		case MeasureType::kSeconds: {
			totalTime += deltaTime;

		}break;
		}
	}
	float GetTotalTime()const { return totalTime; }

private:
	MeasureType type;
	float totalTime;
};

class DelayManager abstract final {
private:
	static std::unordered_map<const char*, Delay> tasks;
public:
	static void Update(float deltaTime);
	static void Execute(const char* name, uint32_t targetTime, Delay::MeasureType type, const std::function<void(void)>& func);
};