#pragma once

//	https://github.com/elanthis/constexpr-hash-demo/blob/master/test.cpp
class FNVHash final {
	static constexpr unsigned long long basis = 14695981039346656037ULL;
	static constexpr unsigned long long prime = 1099511628211ULL;

	static constexpr size_t hash_one(char c, const char* remain, unsigned long long value) {
		return c == 0 ? value : hash_one(remain[0], remain + 1, (value ^ c) * prime);
	}
public:

	static constexpr size_t hash(const char* str) {
		return hash_one(str[0], str + 1, basis);
	}

};
