#pragma once

#include "imgui.h"

//	このあたりの実装は適当
class Model {
};

class View {
public:
	View(const char* name, ImVec2 pos, ImVec2 size);
	virtual ~View();
public:
	enum class State {
		kInvisible,
		kFadeIn,
		kUpdate,
		kFadeOut,
	};
	State GetUIState() const { return state; };

public:
	void StartFadeIn();
	void StartFadeOut();
	void Update();

protected:
	virtual void CoreUpdate() = 0;
	float totalDeltaTime = 0.f;
	const char* viewName = "";

	ImVec2 pos, size;

private:
	State state = State::kFadeIn;
	float alpha = 0.f;
};

class Presenter {
public:
	virtual void Update() = 0;
};
