#include "Singleton.hpp"
#include <cstdlib>

namespace
{
	constexpr size_t MaxFinalizerNum = 256;
	size_t finalizerSize = 0;

	SingletonFinalizer::FinalizerFunc finalizers[MaxFinalizerNum];
}

void SingletonFinalizer::AddFinalizer(FinalizerFunc finalizer)
{
	if (finalizerSize >= MaxFinalizerNum)
	{
		exit(EXIT_FAILURE);
	}

	finalizers[finalizerSize] = finalizer;
	finalizerSize++;
}

void SingletonFinalizer::Finalize()
{
	for (size_t i = finalizerSize; 0 < i; i--)
	{//	生成と逆順に開放するため引き算
		(finalizers[i - 1])();
	}
	finalizerSize = 0;
}