#pragma once
#include <functional>
#include <mutex>
#include <assert.h>

class SingletonFinalizer
{
public:
	using FinalizerFunc = std::function<void()>;

	static void AddFinalizer(FinalizerFunc finalizer);
	static void Finalize();
};

template <typename T>
class Singleton final
{
private:
	static std::once_flag isInit_;
	static T* instance_;

private:
	static void Create()
	{
		instance_ = new T();
		SingletonFinalizer::AddFinalizer(&Singleton<T>::Destroy);
	}

	static void Destroy()
	{
		delete instance_;
		instance_ = nullptr;
	}

public:
	static T* Get()
	{
		std::call_once(isInit_, Create);
		return instance_;
	}
};
template <typename T> std::once_flag Singleton<T>::isInit_;
template <typename T> T* Singleton<T>::instance_ = nullptr;