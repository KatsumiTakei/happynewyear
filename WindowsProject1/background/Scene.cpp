#include "Scene.hpp"
#include "imgui.h"

std::unordered_map<size_t, std::unique_ptr<Scene>> SceneApi::hashMap_;

Scene::Scene(const char* name)
	:name_(name), state_(SceneState::kDead) {
}

Scene::~Scene() {
}

void Scene::Load() {
	if (state_ == SceneState::kDead) {
		state_ = SceneState::kLoading;
	}
}


void Scene::Terminate() {
	if (state_ == SceneState::kUsable) {
		state_ = SceneState::kDead;
	}
}

void Scene::Update() {
	constexpr uint8_t kTransitionTime = 3;
	switch (state_) {
	case SceneState::kDead: {
	}break;
	case SceneState::kLoading: {
		ImGuiManager::Get()->PushContents(kLog, [&] {
			ImGui::Text("%s kLoading...", name_);
		});
		//	テストなので適当に待機
		DelayManager::Execute("SceneLoad", kTransitionTime, Delay::MeasureType::kSeconds, [&]() {
			state_ = SceneState::kUsable;
		});

	}break;
	case SceneState::kUsable: {
		CoreUpdate();
	}break;
	case SceneState::kTerminating: {
		ImGuiManager::Get()->PushContents(kLog, [&] {
			ImGui::Text("%s kTerminating...", name_);
		});
		//	テストなので適当に待機
		DelayManager::Execute("SceneLoad", kTransitionTime, Delay::MeasureType::kSeconds, [&]() {
			state_ = SceneState::kDead;
		});

	}break;
	}
}

void SceneApi::Update() {
	for (const auto& data : hashMap_) {
		data.second->Update();
	}
}