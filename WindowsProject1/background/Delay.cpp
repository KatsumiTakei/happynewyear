#include "Delay.hpp"

std::unordered_map<const char*, Delay> DelayManager::tasks;

void DelayManager::Update(float deltaTime) {
	for (auto& task : tasks) {
		task.second.StepTime(deltaTime);
	}
}

void DelayManager::Execute(const char* name, uint32_t targetTime, Delay::MeasureType type, const std::function<void(void)> &func){
	auto task = tasks.find(name);
	if (task != tasks.end()) {
		if (static_cast<uint32_t>(std::floor(task->second.GetTotalTime())) >= targetTime) {
			func();
			tasks.erase(name);
		}
	}
	else {
		tasks.try_emplace(name, type);
	}
}
