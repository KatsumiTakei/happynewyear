#pragma once

class Application final {
private:
	class Impl;
	std::unique_ptr<Impl> pImpl;

public:
	Application();
	~Application();
public:
	void Run();
};