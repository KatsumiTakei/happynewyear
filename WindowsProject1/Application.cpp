#include "Application.hpp"

#include "sample/UISystemManager.hpp"
#include "sample/ItemGetHudCommands.hpp"
#include "imgui.h"
#include "sample/Inventory.hpp"

namespace {
    std::string itemName = "test";
    void DebugCommandExecuter() {
        ImGui::SeparatorText("ItemGet");
        ImGui::InputText("ItemName", itemName.data(), 256);
        bool isEvent = false;
        ImGui::Checkbox("isEvent ", &isEvent);

        if (ImGui::Button("ItemGetHudOpen")) {
            Inventory::AddItem(itemName.data(), 1, isEvent);
        }
        if (ImGui::Button("ItemGetHudClose")) {
            UISystemManager::Get()->AddCommand<ItemGetHudCloseCommand>();
        }

        ImGui::SeparatorText("Message");
        ImGui::SeparatorText("MapMenu");
        ImGui::SeparatorText("ItemMenu");
    }
}

class Application::Impl {
public:
    void Run() {

        auto input = Singleton<Keyboard>::Get();
        input->RregisterKey(eKey::kUp, VK_UP);
        input->RregisterKey(eKey::kDown, VK_DOWN);
        input->RregisterKey(eKey::kRight, VK_RIGHT);
        input->RregisterKey(eKey::kLeft, VK_LEFT);

        auto fpsTimer = Singleton<QueryPerformanceTimer>::Get();
        auto uiManager = UISystemManager::Get();

        fpsTimer->Wait();
        input->Update();
        DelayManager::Update(fpsTimer->DeltaTime());
        SceneApi::Update();
        uiManager->Update();
        ImGuiManager::Get()->Update();
    }

};

Application::Application()
    :pImpl(std::make_unique<Impl>()) {

    auto input = Singleton<Keyboard>::Get();
    input->RregisterKey(eKey::kSpace, VK_SPACE);

    auto imGuiManager = ImGuiManager::Get();
    imGuiManager->RegistWindow(kMain, { 0, 0 }, { 250, 200 });

    ImGuiManager::Get()->PushContents(kMain, [] {
        DebugCommandExecuter();
    }, false);

    imGuiManager->RegistWindow(kCommandHistory, { 800, 0 }, { 350, 300 });
    ImGuiManager::Get()->PushContents(kCommandHistory, [] {
        UISystem::Debug::DebugDraw();
    }, false);

    imGuiManager->RegistWindow(kLog, { 400, 0 }, { 250, 400 });

}

Application::~Application() {
    SingletonFinalizer::Finalize();
}

void Application::Run() {
    pImpl->Run();
}
