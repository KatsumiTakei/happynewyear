#pragma once

#include <MVP.hpp>
#include <ReactiveProperty.hpp>

class ItemGetPresenterBase;

//	このあたりの実装は適当
class ItemGetModel final : public Model {
	friend ItemGetPresenterBase;
public:
	ItemGetModel() = default;
	~ItemGetModel() = default;

private:
	ReactiveProperty<std::string> itemName;
};
/**
 * @brief 指定座標に指定サイズで文字を表示するだけのView
*/
class ItemGetViewBase : public View {
public:
	ItemGetViewBase(const char* name, ImVec2 pos, ImVec2 size);
	virtual ~ItemGetViewBase();
public:
	void OnChangeViewText(const char* text);
	void TransitionLastViewTime();
	bool IsCanRecieveRequest()const;
protected:
	std::string viewText = "";
	const float kViewTime = 3;
};
/**
 * @brief リクエストを元にシーンからの操作されるクラス
*/
class ItemGetPresenterBase : public Presenter {
public:
	ItemGetPresenterBase(std::unique_ptr<ItemGetViewBase>&& viewBase);
	virtual ~ItemGetPresenterBase();

	bool OnOpenRequest(std::string itemName);
	bool OnCloseRequest();

	bool IsInvisibleView()const;
	bool IsCanRecieveRequest()const;
protected:
	std::unique_ptr<ItemGetModel> model;
	std::unique_ptr<ItemGetViewBase> view;
};