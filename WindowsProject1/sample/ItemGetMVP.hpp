#pragma once

#include "ItemGetHudMVPBase.hpp"

/**
 * @brief 取得済みアイテムの取得時HudのView
*/
class AcquiredItemGetView final : public ItemGetViewBase {
public:
	AcquiredItemGetView();
	~AcquiredItemGetView();
public:
	void CoreUpdate() override;

};

/**
 * @brief 取得済みアイテムの取得時HudのPresenter
*/
class AcquiredItemGetPresenter final : public ItemGetPresenterBase {
public:
	AcquiredItemGetPresenter();
	~AcquiredItemGetPresenter();

	void Update()override;

};


/**
 * @brief 新規アイテムの取得時HudのView
*/
class NewItemGetView final : public ItemGetViewBase {
public:
	NewItemGetView();
	~NewItemGetView();
public:
	void CoreUpdate() override;

};

/**
 * @brief 新規アイテムの取得時HudのPresenter
*/
class NewItemGetPresenter final : public ItemGetPresenterBase {
public:
	NewItemGetPresenter();
	~NewItemGetPresenter();

	void Update()override;

};
