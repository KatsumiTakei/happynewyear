#pragma once

#include <queue>
#include <Scene.hpp>

class ItemGetPresenterBase;
class ItemGetHudSceneBase;
/**
 * @brief UIの操作をするためのリクエスト
*/
class IItemGetHudRequest {
public:
	virtual void OnPrevAdd(ItemGetHudSceneBase* scene) = 0;
	virtual bool Execute(ItemGetPresenterBase* presenter) = 0;
	virtual void OnSuccuss(std::queue<std::unique_ptr<IItemGetHudRequest>>& queue) = 0;
};
/**
 * @brief UIを開くリクエスト
 *		  すでにUIが開いていたらアイテム名のみ更新する
*/
class ItemGetHudOpenRequest : public IItemGetHudRequest {
public:
	void OnPrevAdd(ItemGetHudSceneBase* scene)override;
	bool Execute(ItemGetPresenterBase* presenter)override;
	void OnSuccuss(std::queue<std::unique_ptr<IItemGetHudRequest>>& queue)override;
public:
	std::string itemName = "";
};
/**
 * @brief UIを閉じるリクエスト
*/
class ItemGetHudCloseRequest : public IItemGetHudRequest {
public:
	void OnPrevAdd(ItemGetHudSceneBase* scene)override;
	bool Execute(ItemGetPresenterBase* presenter)override;
	void OnSuccuss(std::queue<std::unique_ptr<IItemGetHudRequest>>& queue)override;
};

class ItemGetHudSceneBase : public Scene {

public:
	ItemGetHudSceneBase(const char* name, std::unique_ptr<ItemGetPresenterBase>&& presenterBase);
	virtual ~ItemGetHudSceneBase();
public:
	template<class T>
	void AddRequest(T&& param) {
		static_assert(std::is_base_of<IItemGetHudRequest, T>::value, "ItemGetHud AddRequest : not IItemGetHudRequest");
		auto request = std::make_unique<T>(std::forward<T>(param));
		request->OnPrevAdd(this);
		requests.emplace(std::move(request));
	}
	void ClearRequest();
private:
	void CoreUpdate()override;
protected:
	std::queue<std::unique_ptr<IItemGetHudRequest>> requests;
	std::unique_ptr<ItemGetPresenterBase> presenter;
};
/**
 * @brief 入手済みアイテムのUIシーン
*/
class AcquiredItemGetHudScene final : public ItemGetHudSceneBase {

public:
	AcquiredItemGetHudScene();
	~AcquiredItemGetHudScene();

};
/**
 * @brief 新規アイテムのUIシーン
*/
class NewItemGetHudScene final  : public ItemGetHudSceneBase {
public:
	NewItemGetHudScene();
	~NewItemGetHudScene();

};