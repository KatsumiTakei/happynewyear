#include "UISystemManager.hpp"

#include "Command.hpp"
#include "ItemGetHudCommands.hpp"
#include "imgui.h"

#pragma region	//	ここは本質でないので無視してくだされ…
std::vector<std::pair<std::string, ICommand*>> UISystem::Debug::history;

void UISystem::Debug::Regist(ICommand* command) {

	history.emplace_back(ConvertName(command), command);
}

void UISystem::Debug::OnDeadCommand(ICommand* command) {
	for (auto& content : Debug::history) {
		if (content.second == command) {
			content.second = nullptr;
			break;
		}
	}
}

std::string UISystem::Debug::ConvertName(ICommand* command) {
	std::string name = "";

	switch (command->GetHash()) {
	case ItemGetHudOpenCommand::kHash: {
		name = "ItemGetHudOpenCommand";
	}break;
	case ItemGetHudCloseCommand::kHash: {
		name = "ItemGetHudCloseCommand";
	}break;
	}
	return name;
}

void UISystem::Debug::DebugDraw() {

	ImGui::TextColored(ImVec4(1.0, 1.0, 0.0, 1.0), "Commands Queue");
	ImGui::BeginChild("Scrolling", ImVec2(300, 100), ImGuiWindowFlags_NoTitleBar);
	for (int i = 0; i < UISystemManager::Get()->commands_.size(); i++) {
		const std::string kName = Debug::ConvertName(UISystemManager::Get()->commands_[i].get());
		ImGui::Text("%s_%d", kName.c_str(), i);
	}
	ImGui::EndChild();


	if (ImGui::CollapsingHeader("History")) {

		const auto& history = UISystem::Debug::history;
		for (int i = 0; i < history.size(); i++) {
			ImGui::Text("%s_%d : ", history[i].first.c_str(), i);
			ImGui::SameLine();
			//	お行儀が悪いが面倒なのでポインタの生死をみてテキストの色を変える
			if (history[i].second) {
				ImGui::TextColored(ImVec4(0.0, 1.0, 0.0, 1.0), "active");
			}
			else {
				ImGui::TextColored(ImVec4(1.0, 0.0, 0.0, 1.0), "dead");
			}
		}
	}

}
#pragma endregion

UISystem::UISystem()
	: commands_() {
}

UISystem::~UISystem() {
}

void UISystem::Update() {
	for (auto it = commands_.begin(); it != commands_.end();) {
		if ((*it)->Execute() == CommandState::kContinue) {
			++it;
			continue;
		}
		Debug::OnDeadCommand((*it).get());

		it = commands_.erase(it);
	}
}

bool UISystem::IsExistCommand(size_t commandHash) {
	return std::find_if(commands_.begin(), commands_.end(),
		[commandHash](const std::unique_ptr<ICommand>& command) { return command->GetHash() == commandHash; }) != commands_.end();
}

