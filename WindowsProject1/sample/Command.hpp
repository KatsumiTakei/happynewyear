#pragma once
#include "FNVHash.hpp"

/**
 * @brief 
*/
enum class CommandState {
	kContinue,
	kSuccess,
	kFailed,
	kAbort,
};

/**
 * @brief コマンドのインターフェース
*/
class ICommand abstract {
public:
	//	コピーとムーブは必要ないのでつぶす
	ICommand(const ICommand&) = delete;
	ICommand(ICommand&&) = delete;
	ICommand() = default;
	/**
	 * @brief コマンドの実行部分
	 *		　マネージャーから定期実行される
	 * @return コマンドの状態
	*/
	virtual CommandState Execute() = 0;
	/**
	 * @brief ハッシュの取得
	 *		  コマンドの判別とかに利用する想定
	 * @return ハッシュ
	*/
	virtual size_t GetHash()const = 0;

};


//#define USE_UICOMMAND_CONSTRUCT

//	ボイラーテンプレートが多いので実装を簡単にするマクロ
#ifdef USE_UICOMMAND_CONSTRUCT
#define CONSTRUCT_COMMAND(className) public:\
	static constexpr size_t kHash = FNVHash::hash(#className);\
	size_t GetHash()const override { return kHash; }\
	CommandState Execute() override;

#define CONSTRUCT_COMMAND_WITH_PARAMS(className, Params) CONSTRUCT_COMMAND(className)\
	className(Params&& params):params_(std::forward<Params>(params)) {}\
	private:\
		Params params_;

#define CONSTRUCT_COMMAND_PARAMS(className) using value_type = className;\
		Params(const Params&) = delete;\
		Params() = default;\
		Params(Params&&) = default;
#endif
