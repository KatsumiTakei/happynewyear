#include "ItemGetMVP.hpp"


AcquiredItemGetView::AcquiredItemGetView()
	: ItemGetViewBase("AcquiredItemGetView", ImVec2(1000, 400), ImVec2(300, 100)) {
}

AcquiredItemGetView::~AcquiredItemGetView() {
}

void AcquiredItemGetView::CoreUpdate() {
	//	アイテム名と残りの表示秒数を表示
	ImGui::Text(viewText.c_str());
	if (GetUIState() == State::kUpdate) {
		ImGui::Text("remain view time %.2f", kViewTime - totalDeltaTime);
	}
}

AcquiredItemGetPresenter::AcquiredItemGetPresenter() 
	: ItemGetPresenterBase(std::make_unique<AcquiredItemGetView>()){
}

AcquiredItemGetPresenter::~AcquiredItemGetPresenter(){
}

void AcquiredItemGetPresenter::Update() {
	view->Update();
}

NewItemGetView::NewItemGetView()
	: ItemGetViewBase("NewItemGetView", ImVec2(150, 500), ImVec2(1000, 300)) {
}

NewItemGetView::~NewItemGetView() {
}

void NewItemGetView::CoreUpdate() {

	//	アイテム名と残りの表示秒数を表示
	//	Space押せることも表示
	ImGui::Text(viewText.c_str());
	if (GetUIState() == State::kUpdate) {
		ImGui::Text("remain view time %.2f", kViewTime - totalDeltaTime);
		ImGui::Text("Can Press Space!!!!!!!!!!!");
	}
}

NewItemGetPresenter::NewItemGetPresenter()
	: ItemGetPresenterBase(std::make_unique<NewItemGetView>()) {
}

NewItemGetPresenter::~NewItemGetPresenter() {
}

void NewItemGetPresenter::Update() {
	if (Singleton<Keyboard>::Get()->IsTouched(eKey::kSpace)) {
		//	space押したらViewの表示時間を完了にする
		//	他にリクエストがあればそちらに移行してなければ終了に遷移する
		view->TransitionLastViewTime();
	}
	view->Update();
}
