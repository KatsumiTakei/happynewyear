#include "ItemGetHudScene.hpp"
#include "ItemGetMVP.hpp"

void ItemGetHudOpenRequest::OnPrevAdd(ItemGetHudSceneBase* scene) {
	//	一定以上のリクエストがあればどこかを削除するとかキューに積まないようにするとかできる
}

bool ItemGetHudOpenRequest::Execute(ItemGetPresenterBase* presenter) {

	return presenter->OnOpenRequest(itemName);
}

void ItemGetHudOpenRequest::OnSuccuss(std::queue<std::unique_ptr<IItemGetHudRequest>>& queue){
	queue.pop();
}

void ItemGetHudCloseRequest::OnPrevAdd(ItemGetHudSceneBase* scene) {
	//	強制終了をすぐに実行できるようにUIにたまっているリクエストをクリアしておく
	//	コマンドは消去しないので、この後にOpenコマンドが積まれると再表示される
	//	強制終了後の再表示が好ましくないならIsCanAddやOnPrevAddなどでコマンドが積まれないようにする
	scene->ClearRequest();
}

bool ItemGetHudCloseRequest::Execute(ItemGetPresenterBase* presenter) {
	return presenter->OnCloseRequest();
}

void ItemGetHudCloseRequest::OnSuccuss(std::queue<std::unique_ptr<IItemGetHudRequest>>& queue){
	//	基本はpopでよいが、何かしらでクリアしたい時はこの関数内で挙動を変更する
	//	他にもイベント的に発火したい関数があればそれも呼ぶ
	std::queue<std::unique_ptr<IItemGetHudRequest>> temp;
	queue.swap(temp);
	//queue.pop();
}

ItemGetHudSceneBase::ItemGetHudSceneBase(const char* name, std::unique_ptr<ItemGetPresenterBase>&& presenterBase)
	: Scene(name), requests(), presenter(std::move(presenterBase)){
}

ItemGetHudSceneBase::~ItemGetHudSceneBase() {
}

void ItemGetHudSceneBase::ClearRequest() {
	std::queue<std::unique_ptr<IItemGetHudRequest>> temp;
	requests.swap(temp);
}

void ItemGetHudSceneBase::CoreUpdate() {
	if (requests.empty()) {
		if (presenter->IsCanRecieveRequest()){
			//	シーン内のリクエストがないならPresenterに終了を通知
			presenter->OnCloseRequest();
		}
		if (presenter->IsInvisibleView()) {
			//	リクエストない状態で非表示ならシーンを破棄する
			//	シーンにアクセサがあるならそちら経由で破棄する
			Terminate();
		}
	} else {
		//	リクエストがあれば実行してみて成功したらイベントも実行する
		const auto& request = requests.front();
		if (request->Execute(presenter.get())) {
			request->OnSuccuss(requests);
		}
	}
	presenter->Update();
}

AcquiredItemGetHudScene::AcquiredItemGetHudScene()
	:ItemGetHudSceneBase("AcquiredItemGetHudScene", std::make_unique<AcquiredItemGetPresenter>()) {
}

AcquiredItemGetHudScene::~AcquiredItemGetHudScene() {
}

NewItemGetHudScene::NewItemGetHudScene()
	:ItemGetHudSceneBase("NewItemGetHudScene", std::make_unique<NewItemGetPresenter>()) {
}

NewItemGetHudScene::~NewItemGetHudScene() {
}
