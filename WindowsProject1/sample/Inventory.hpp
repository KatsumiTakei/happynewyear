#pragma once

//	アイテム情報の保存
//	これも適当です
struct InventoryData final {
	InventoryData(std::string itemName, bool isNew, uint8_t num);
	~InventoryData();
	std::string itemName = "";
	bool isNew = true;
	uint16_t num = 0;
};

class Inventory final {

public:
	static void AddItem(std::string itemName, uint16_t num, bool isEvent);
	static bool IsNewItem(std::string itemName);
private:
	static std::list<InventoryData> data;
};