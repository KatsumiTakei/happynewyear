#include "Inventory.hpp"

#include "UISystemManager.hpp"
#include "ItemGetHudCommands.hpp"

std::list<InventoryData> Inventory::data;


InventoryData::InventoryData(std::string itemName, bool isNew, uint8_t num)
	: itemName(itemName), isNew(isNew), num(num) {
}

InventoryData::~InventoryData(){
}


void Inventory::AddItem(std::string itemName, uint16_t num, bool isEvent = false) {
	ItemGetHudOpenCommand::Params params;

	params.modelParam.itemName = itemName;
	params.isNewItem = IsNewItem(itemName);
	params.isEvent = isEvent;
	UISystemManager::Get()->AddCommand(std::move(params));

	auto itemData = std::find_if(data.begin(), data.end(), [&](const InventoryData& itemData) { return itemData.itemName == itemName; });
	if (itemData != data.end()) {
		itemData->num += num;
	}
	else {
		data.emplace_back(itemName, num, false);
	}
}
bool Inventory::IsNewItem(std::string itemName) {
	for (const auto& item : data) {
		if (itemName == item.itemName) {
			return false;
		}
	}
	return true;
}
