#include "ItemGetHudCommands.hpp"
#include "UISystemManager.hpp"
#include "ItemGetHudScene.hpp"
#include "imgui.h"

CommandState ItemGetHudOpenCommand::Execute() {

	ItemGetHudSceneBase* hudScene = nullptr;
	if (params_.isNewItem) {
		hudScene = SceneApi::FindScene<NewItemGetHudScene>("NewItemGetHudScene");
	} else {
		hudScene = SceneApi::FindScene<AcquiredItemGetHudScene>("AcquiredItemGetHudScene");
	}

	//	UIにアクセスできるようになるまではコマンド内で処理する
	switch (hudScene->GetSceneState()) {
	case SceneState::kDead: {
		//	シーンがなければ読み込み
		hudScene->Load();
		return CommandState::kContinue;
	}break;
	case SceneState::kLoading: {
		//	読み込み中ならスタック
		return CommandState::kContinue;

	}break;
	case SceneState::kUsable: {
		//	シーンが利用可能になったらUIに対して表示用のリクエストをしてコマンドを完了
		//	この辺は実装に合わせる
		ItemGetHudOpenRequest request;
		request.itemName = params_.modelParam.itemName;
		hudScene->AddRequest(std::move(request));
	}break;
	case SceneState::kTerminating: {
		//	シーンが破棄中なら破棄されきるまでコマンドを待機
		return CommandState::kContinue;
	}break;
		
	} 

	return CommandState::kSuccess;
}

bool ItemGetHudCloseCommand::IsCanAdd() {
	//	すでにクローズが積まれていたら必要ないのでキューには積まないとかできる
	//return !UISystemManager::Get()->IsExistCommand(kHash);
	return true;
}

CommandState ItemGetHudCloseCommand::Execute() {
	//	アイテム取得Hudは2種類あるから両方破棄するまで継続する
	if (!StepCommand(SceneApi::FindScene<AcquiredItemGetHudScene>("AcquiredItemGetHudScene")) || 
		!StepCommand(SceneApi::FindScene<NewItemGetHudScene>("NewItemGetHudScene"))) {
		return CommandState::kContinue;
	}

	return CommandState::kSuccess;
}

bool ItemGetHudCloseCommand::StepCommand(ItemGetHudSceneBase* hudScene) {
	//	UIにアクセスできるようになるまではコマンド内で処理する
	switch (hudScene->GetSceneState()) {
	case SceneState::kDead: {
		//	すでに閉じてたら完了
	}break;
	case SceneState::kLoading: {
		//	ローディング中なら待機
		return false;
	}break;
	case SceneState::kUsable: {


		//	シーンが有効になっていたらリクエストに積んで実行タイミングはUIに任せる
		// （アニメとかあるので）
		//	コマンド自体はここで完了する
		hudScene->AddRequest(ItemGetHudCloseRequest());
	}break;
	case SceneState::kTerminating: {
		//	シーンが破棄中なら破棄されきるまでコマンドを待機
		//	CloseCommandの前にOpenCommandが積まれている可能性があるため
		return false;

	}break;

	}
	return true;
}
