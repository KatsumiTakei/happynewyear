#pragma once
#include "Command.hpp"

#include <string>

class ItemGetHudSceneBase;

struct ItemGetHudModelParam final {
	ItemGetHudModelParam(const ItemGetHudModelParam&) = delete;
	ItemGetHudModelParam() = default;
	ItemGetHudModelParam(ItemGetHudModelParam&&) = default;
	std::string itemName = "";
};

/**
 * @brief Hudを開くコマンド
*/
#ifdef USE_UICOMMAND_CONSTRUCT
class ItemGetHudOpenCommand final : public ICommand {

public:
	struct Params {
		CONSTRUCT_COMMAND_PARAMS(ItemGetHudOpenCommand)

		//	UIには必要ないがコマンド内で利用したいデータ
		bool isNewItem = true;
		bool isEvent = false;
		//	UIに渡すデータ
		ItemGetHudModelParam modelParam;
	};

	CONSTRUCT_COMMAND_WITH_PARAMS(ItemGetHudOpenCommand)

};
#else
class ItemGetHudOpenCommand final : public ICommand {
public:
	struct Params {
		using value_type = ItemGetHudOpenCommand;

		//	コピーは必要ないのでつぶす
		//	この辺は面倒だが、やっておいたほうが意図しないコードは混入しずらい
		//	大きい構造体だけでも良いが、拡張により大きくなるとかだと気づきにくいので
		//	最初から全部やったほうがよさげ
		Params(const Params&) = delete;
		Params() = default;
		Params(Params&&) = default;

		//	UIには必要ないがコマンド内で利用したいデータ
		bool isNewItem = true;
		bool isEvent = false;
		//	UIに渡すデータ
		ItemGetHudModelParam modelParam;
	};

	ItemGetHudOpenCommand(Params&& params)
		:params_(std::forward<Params>(params)) {
	}

	static constexpr size_t kHash = FNVHash::hash("ItemGetHudOpenCommand");
	size_t GetHash()const override {
		return kHash;
	}
	CommandState Execute() override;


private:
	Params params_;
};
#endif

/**
 * @brief Hudを強制的に閉じるコマンド
*/
#ifdef USE_UICOMMAND_CONSTRUCT
class ItemGetHudCloseCommand final : public ICommand {

	static bool IsCanAdd();
	CONSTRUCT_COMMAND(ItemGetHudCloseCommand)
};
#else
class ItemGetHudCloseCommand final : public ICommand {
public:
	static bool IsCanAdd();
	static constexpr size_t kHash = FNVHash::hash("ItemGetHudCloseCommand");
	size_t GetHash()const override {
		return kHash;
	}
	CommandState Execute() override;
	bool StepCommand(ItemGetHudSceneBase* hudScene);
};
#endif
