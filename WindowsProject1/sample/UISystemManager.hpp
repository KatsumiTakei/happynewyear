#pragma once
#include <memory>
#include <vector>
#include <type_traits>

class ICommand;

template<typename T>
class Singleton;

class UISystem final {

public:
	
#pragma region	// ここは本質でないので無視してくだされ…
	class Debug final {
	public:

		static std::vector<std::pair<std::string, ICommand*>> history;
		static void Regist(ICommand* command);
		static void OnDeadCommand(ICommand* command);
		static std::string ConvertName(ICommand* command);
		static void DebugDraw();
	};
#pragma endregion


public:
	friend Singleton<UISystem>;

	void Update();

	/**
	 * @brief コマンドがキューに存在するかチェック
	 * @param commandHash 
	 * @return 
	*/
	bool IsExistCommand(size_t commandHash);

	/**
	 * @brief コマンドをキューに積む
	 * @tparam T !<	発行するコマンドの型（パラメーターなし）
	*/
	template<class T>
	void AddCommand() {
		static_assert(std::is_base_of<ICommand, T>::value, "PushCommand : not ICommand");

		if (!IsCanAdd<T>()) {
			return;
		}

		commands_.emplace_back(std::make_unique<T>());
		Debug::Regist(commands_[commands_.size() - 1].get());
	}
	/**
	 * @brief 
	 * @tparam T !<	コマンドに必要なパラメーター
	 * @tparam U !<	パラメーターで定義したコマンド型
	 *				インターフェースを合わせたいとか、
	 *				パラメーターで定義するのが面倒なら利用側に指定させてもよい
	 * @param params 
	*/
	template<typename T, class U = typename T::value_type>
	void AddCommand(T&& params) {
		static_assert(std::is_constructible<U, T>::value, "PushCommand : not ICommand for params");
		
		if (!IsCanAdd<U>()) {
			return;
		}

		commands_.emplace_back(std::make_unique<U>(std::forward<T>(params)));
		Debug::Regist(commands_[commands_.size() - 1].get());
	}
private:
	UISystem();
	~UISystem();

private:
	/**
	 * @brief コマンドをキューに積むか判定する
	 * @tparam T !<	キューに積むコマンド型
	 * @return キューに積むか
	*/
	template<class T>
	bool IsCanAdd() {
		if constexpr (has_IsCanAdd<T>::value) {
			return T::IsCanPush();
		}
		return true;
	}
	/**
	 * @brief SFINAEで指定の型にIsCanAdd関数が存在するかチェックするメタ関数
	 * この辺は分かりずらいならICommandにから実装のIsCanAddを持たせるでもよい
	 * @tparam T !<	キューへの追加に条件があるコマンド
	 * @note https://theolizer.com/cpp-school2/cpp-school2-10/
	*/
	template<class T>
	struct has_IsCanAdd {

		//	dummyで指定したメンバが存在しなければ解決されないので他のオーバーロードされた関数も評価する
		//	decltypeで使用したいだけなので関数定義は不要
		template<class Target, int dummy = (&Target::IsCanAdd, 0)>
		static std::true_type  check(Target*);
		//	他のオーバーロードされた関数がエラーになった場合はこれがコールされる
		//	decltypeで使用したいだけなので関数定義は不要
		static std::false_type check(...);

		//	テンプレートで受けた型を上記の関数に渡すので、SFINAEによってコンパイル時にbool値が決まる
		static constexpr bool value = decltype(check(std::declval<T>()))::value;
	};


private:
	std::vector<std::unique_ptr<ICommand>> commands_;

};

using UISystemManager = Singleton<UISystem>;