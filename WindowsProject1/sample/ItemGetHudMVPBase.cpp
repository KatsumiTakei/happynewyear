#include "ItemGetHudMVPBase.hpp"


ItemGetViewBase::ItemGetViewBase(const char* name, ImVec2 pos, ImVec2 size)
	: View(name, pos, size) {
}

ItemGetViewBase::~ItemGetViewBase() {
}
void ItemGetViewBase::OnChangeViewText(const char* text) {
	viewText = std::string("Get Item : ") + text;
	totalDeltaTime = 0;
}
void ItemGetViewBase::TransitionLastViewTime() {
	totalDeltaTime = kViewTime;
}
bool ItemGetViewBase::IsCanRecieveRequest()const {
	return totalDeltaTime >= kViewTime;
}

ItemGetPresenterBase::ItemGetPresenterBase(std::unique_ptr<ItemGetViewBase>&& viewBase)
	: model(std::make_unique<ItemGetModel>()), view(std::move(viewBase)) {
	model->itemName.Subscribe([&](std::string itemName) { view->OnChangeViewText(itemName.c_str()); });
}
ItemGetPresenterBase::~ItemGetPresenterBase() {
}

bool ItemGetPresenterBase::OnOpenRequest(std::string itemName) {
	if (IsInvisibleView()) {
		//	非表示ならフェードイン開始
		view->StartFadeIn();
		model->itemName.OnNext(itemName);
		return true;

	} else if (view->GetUIState() == View::State::kUpdate && view->IsCanRecieveRequest()) {
		//	すでに表示中であればアイテム名のみ変更
		model->itemName.OnNext(itemName);
		return true;
	}

	return false;
}
bool ItemGetPresenterBase::OnCloseRequest() {
	if (view->GetUIState() == View::State::kUpdate) {
		view->StartFadeOut();
		return true;
	} else if (IsInvisibleView()) {
		return true;
	}
	return false;
}

bool ItemGetPresenterBase::IsInvisibleView()const {
	return view->GetUIState() == View::State::kInvisible;
}

bool ItemGetPresenterBase::IsCanRecieveRequest() const {
	return view->IsCanRecieveRequest();
}
